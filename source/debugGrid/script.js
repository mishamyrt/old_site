import $ from 'mishamyrt-noquery'

let node = null
let offsetY = 0
let isVisible = false

export default class DebugGrid {
    static init (gridNode) {
        node = gridNode
        document.addEventListener('keydown', (e) => this.keyDown(e))
        $('.footer-dash').onclick = () => this.toggle()
        $('.debugGrid-plus', gridNode).onclick = this.plus
        $('.debugGrid-minus', gridNode).onclick = this.minus
    }
    static toggle () {
        if (isVisible) {
            node.style.display = 'none'
        } else {
            node.style.display = 'block'
        }
        isVisible = !isVisible
    }
    static plus () {
        offsetY++
        node.style.top = offsetY + 'px'
    }
    static minus () {
        offsetY--
        node.style.top = offsetY + 'px'
    }
    static keyDown (e) {
        const meta = navigator.platform.match('Mac') ? e.metaKey : e.ctrlKey
        if (e.keyCode === 71 && meta) {
            e.preventDefault()
            this.toggle()
        } else if (isVisible && (e.keyCode === 40 || e.keyCode === 38)) {
            switch (e.keyCode) {
                case 40:
                    this.plus()
                    break
                case 38:
                    this.minus()
                    break
                default:
                    break
            }
            e.preventDefault()
        }
    }
}
