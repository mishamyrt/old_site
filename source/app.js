import $ from 'mishamyrt-noquery'
import DebugGrid from './debugGrid/script'
import Honeymate from 'mishamyrt-honeymate'
import ProjectsSlides from './projectsSlides/script'

const slides = $('.projectsSlides-item')
if (slides) {
    ProjectsSlides.init(slides)
}
DebugGrid.init($('.debugGrid'))
if ($('.project')) {
    Honeymate.initiate()
}
