/* eslint no-bitwise: "off" */
const mulTable = [149]
const shgTable = [16]

export default (image, radius, size) => {
    const {
        width,
        height,
    } = size

    const canvas = document.createElement('canvas')

    canvas.width = width
    canvas.height = height

    const context = canvas.getContext('2d')
    context.drawImage(image, 0, 0, width, height)
    boxBlurCanvasRGB(canvas, radius, 2)
    return canvas.toDataURL()
}

function boxBlurCanvasRGB (canvas, radius, iterations) {
    let iter = iterations
    const {
        width,
        height,
    } = canvas
    const context = canvas.getContext('2d')
    const imageData = context.getImageData(0, 0, width, height)

    var pixels = imageData.data

    var rsum
    var gsum
    var bsum
    var x
    var y
    var i
    var p
    var p1
    var p2
    var yp
    var yi
    var yw
    var wm = width - 1
    var hm = height - 1
    var rad1 = radius + 1

    const r = []
    const g = []
    const b = []

    const mulSum = mulTable[0]
    const shgSum = shgTable[0]

    var vmin = []
    var vmax = []

    while (iter-- > 0) {
        yw = yi = 0

        for (y = 0; y < height; y++) {
            rsum = pixels[yw] * rad1
            gsum = pixels[yw + 1] * rad1
            bsum = pixels[yw + 2] * rad1

            for (i = 1; i <= radius; i++) {
                p = yw + (((i > wm ? wm : i)) << 2)
                rsum += pixels[p++]
                gsum += pixels[p++]
                bsum += pixels[p++]
            }

            for (x = 0; x < width; x++) {
                r[yi] = rsum
                g[yi] = gsum
                b[yi] = bsum

                if (y === 0) {
                    vmin[x] = ((p = x + rad1) < wm ? p : wm) << 2
                    vmax[x] = ((p = x - radius) > 0 ? p << 2 : 0)
                }

                p1 = yw + vmin[x]
                p2 = yw + vmax[x]

                rsum += pixels[p1++] - pixels[p2++]
                gsum += pixels[p1++] - pixels[p2++]
                bsum += pixels[p1++] - pixels[p2++]

                yi++
            }
            yw += (width << 2)
        }

        for (x = 0; x < width; x++) {
            yp = x
            rsum = r[yp] * rad1
            gsum = g[yp] * rad1
            bsum = b[yp] * rad1

            for (i = 1; i <= radius; i++) {
                yp += (i > hm ? 0 : width)
                rsum += r[yp]
                gsum += g[yp]
                bsum += b[yp]
            }

            yi = x << 2
            for (y = 0; y < height; y++) {
                pixels[yi] = (rsum * mulSum) >>> shgSum
                pixels[yi + 1] = (gsum * mulSum) >>> shgSum
                pixels[yi + 2] = (bsum * mulSum) >>> shgSum

                if (x === 0) {
                    vmin[y] = ((p = y + rad1) < hm ? p : hm) * width
                    vmax[y] = ((p = y - radius) > 0 ? p * width : 0)
                }

                p1 = x + vmin[y]
                p2 = x + vmax[y]

                rsum += r[p1] - r[p2]
                gsum += g[p1] - g[p2]
                bsum += b[p1] - b[p2]

                yi += width << 2
            }
        }
    }
    context.putImageData(imageData, 0, 0)
}
