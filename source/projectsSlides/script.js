import $ from 'mishamyrt-noquery'
import VanillaTilt from './lib/vanilla-tilt'
import blur from './lib/imageBlur'
import waitImage from './lib/waitImage'

const tiltConfig = {
    max: 7,
    glare: false,
    speed: 1400,
    scale: 1.045,
}

const BLANK_GIF = 'url(data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)'

const calculatePadding = (node) => {
    return window.innerWidth > 500 ? (node.parentNode.offsetHeight / node.offsetWidth) * 100 + '%' : '100%'
}

export default class ProjectsSlides {
    static init (slideNodes) {
        slideNodes.forEach((node) => {
            const src = node.getAttribute('data-src')
            const imageNode = $('.projectsSlides-image', node)
            const placeholderNode = $('.projectsSlides-placeholder', node)
            let loaded = false
            placeholderNode.style.backgroundImage = 'url(' + src.replace('slide', 'slide-thumbnail') + ')'
            waitImage(src.replace('slide', 'slide-thumbnail')).then(
                (img) => {
                    if (!loaded) {
                        placeholderNode.style.opacity = 1
                        placeholderNode.style.backgroundImage = 'url(' + blur(img, 10, {
                            width: 300,
                            height: 200,
                        }) + ')'
                    }
                }
            )
            imageNode.style.backgroundImage = `url(${src})`
            waitImage(src).then(() => {
                imageNode.style.opacity = 1
                loaded = true
                placeholderNode.style.opacity = 0
                setTimeout(() => {
                    placeholderNode.style.backgroundImage = BLANK_GIF
                }, 1000)
            })
            if (imageNode.style.paddingBottom === '') {
                const padding = calculatePadding(node)
                imageNode.style.paddingBottom = padding
            }
            node.onmouseover = () => {
                node.classList.add('__hover')
            }
            node.onmouseout = () => {
                node.classList.remove('__hover')
            }
            node.parentNode.onfocus = node.onmouseover
            node.parentNode.onblur = node.onmouseout
            if (window.innerWidth > 800) {
                VanillaTilt.init(node, tiltConfig)
            }
        })
    }
}
