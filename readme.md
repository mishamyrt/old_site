# Misha
<img src="screenshot.png"
     alt="Screenshot">

## Install

Node.js version not lower than 10 is required, dependencies are installed using the `npm install` command.

### Commands

1. Development: `npm start` - automatic assembly with a watch and a local server.
2. Test: `npm run check-codestyle` - check for code compliance with clearance standards.
3. Build: `npm run build` - build version for production to `/build` folder.

## Deploy

Publication takes place automatically by GitLab CI. All commits from the master branch are sent to production.

## Pictures

- Project slides: 1920 × 1200 Pixel
- Project images: more than 1920 pixels wide

All images should be compressed.