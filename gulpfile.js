/* eslint-env node */
const { src, dest, watch, series, parallel } = require('gulp')
const browserSync = require('browser-sync').create()
const postcss = require('gulp-postcss')
const rollup = require('gulp-rollup')
const rm = require('gulp-rm')
const terser = require('gulp-terser')
const rename = require('gulp-rename')
const imageResize = require('gulp-image-resize')
const gulpif = require('gulp-if')
const csso = require('gulp-csso')

const templates = require('./scripts/build-html')

const importPlugin = require('postcss-import')
const envPlugin = require('postcss-preset-env')
const normalizePlugin = require('postcss-normalize')

const commonjsPlugin = require('rollup-plugin-commonjs')
const resolvePlugin = require('rollup-plugin-node-resolve')
const amdPlugin = require('rollup-plugin-amd')

const PRODUCTION = process.env.NODE_ENV === 'production'

const paths = {
    source: 'source/',
    build: 'build/',
    assets: 'static/',
    templates: 'templates/',
}

const server = () => {
    browserSync.init({
        server: {
            baseDir: paths.build,
        },
        ui: false,
        serveStaticOptions: {
            route: '/',
            dir: paths.build,
        },
        notify: false,
    })
}

const css = () => src(`${paths.source}/styles.css`)
    .pipe(postcss([
        importPlugin({
            root: paths.source,
        }),
        envPlugin({
            stage: 0,
        }),
        normalizePlugin(),
    ]))
    .pipe(gulpif(PRODUCTION, csso()))
    .pipe(dest(paths.build))

const js = () => src(`${paths.source}**/*.js`)
    .pipe(rollup({
        allowRealFiles: true,
        input: `${paths.source}app.js`,
        output: {
            file: `${paths.build}app.js`,
            format: 'iife',
        },
        plugins: [
            resolvePlugin(),
            commonjsPlugin(),
            amdPlugin(),
        ],
    }))
    .pipe(gulpif(PRODUCTION, terser({})))
    .pipe(dest(paths.build))

const html = (done) => templates(PRODUCTION).then(() => done)

const clear = () => src(`${paths.build}**/*`,
    { read: false })
    .pipe(rm())

const copyStatic = () => {
    return src(`${paths.assets}**/*`)
        .pipe(dest(paths.build))
}

const generateThumbnails = () => {
    return src(`${paths.build}projects/**/slide.jpg`)
        .pipe(imageResize({
            width: 67,
            height: 42,
        }))
        .pipe(rename((path) => {
            path.basename += '-thumbnail'
        }))
        .pipe(dest(`${paths.build}projects/`))
}

const rebuild = () => {
    const watchCSS = series(
        css,
        (done) => browserSync.reload() && done()
    )
    const watchJS = series(
        js,
        (done) => browserSync.reload() && done()
    )
    const watchTwig = series(
        html,
        (done) => browserSync.reload() && done()
    )
    watch(`${paths.source}**/*.css`)
        .on('change', watchCSS)
        .on('unlink', watchCSS)
    watch(`${paths.source}**/*.js`)
        .on('change', watchJS)
        .on('unlink', watchJS)
    watch(`${paths.templates}**/*.twig`)
        .on('change', watchTwig)
        .on('unlink', watchTwig)
}

const build = series(
    clear,
    copyStatic,
    generateThumbnails,
    parallel(
        js,
        css
    ),
    html
)

module.exports = {
    rebuild: rebuild,
    css: css,
    js: js,
    html: html,
    server: server,
    clear: clear,
    assets: copyStatic,
    thumbs: generateThumbnails,
    build: build,
    dev: series(
        build,
        parallel(
            server,
            rebuild
        )
    ),
}
