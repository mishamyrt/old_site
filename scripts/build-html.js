/* eslint-env node */
/* eslint no-console: "off"*/
const {
    findGroups,
    render,
    fixTypography,
} = require('./helpers')
const fs = require('fs').promises
const minify = require('html-minifier').minify

const minifyOptions = {
    collapseWhitespace: true,
    removeComments: true,
}


const projectDir = './templates/projects/'
const buildDir = './build/projects/'

const renderProject = (projectGroup, template, production) => {
    return new Promise((resolve) => {
        const group = projectGroup.children
        const link = template.split('.')[0]
        console.log(`Rendering ${link}...`)
        render(projectDir + template, {
            grouplings: group.map(
                (child) => ({
                    title: child.title,
                    link: child.link === link ? '' : child.link,
                })
            ),
            projectTitle: projectGroup.name,
        })
            .then((html) => {
                const processedHtml = fixTypography(html)
                return fs.writeFile(
                    buildDir + link + '/index.html',
                    production ? minify(
                        processedHtml,
                        minifyOptions
                    ) : processedHtml
                )
            })
            .then(() => {
                resolve()
            })
    })
}

module.exports = (production) => {
    return new Promise((resolve) => {
        console.log('Finding groups...')
        findGroups(projectDir)
            .then((groups) => {
                fs.readdir(projectDir)
                    .then((files) => {
                        const promises = []
                        files.forEach((project) => {
                            promises.push(
                                renderProject(groups[
                                    project.split('-')[0]
                                ], project, production)
                            )
                        })
                        return promises
                    })
                    .then((promises) => Promise.all(promises))
                    .then(() => {
                        console.log('Rendering index...')
                        return render('./templates/home.twig').then((html) => {
                            fs.writeFile(
                                './build/index.html',
                                production ? minify(html, minifyOptions) : html
                            )
                        })
                    })
                    .then(() => {
                        console.log('Rendering 404...')
                        return render('./templates/404.twig').then((html) => {
                            fs.writeFile(
                                './build/404.html',
                                production ? minify(html, minifyOptions) : html
                            )
                        })
                    })
                    .then(() => {
                        resolve()
                    })
            })
    })
}
