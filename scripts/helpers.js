/* eslint-env node */
/* eslint no-sync: "off" */
const fs = require('fs').promises
const stat = require('fs').statSync
const cheerio = require('cheerio')
const Twig = require('twig')
const perenoska = require('mishamyrt-perenoska')
const Typograf = require('typograf')
const tp = new Typograf({
    locale: ['ru', 'en-US'],
    htmlEntity: {
        type: 'name',
        onlyInvisible: true,
    },
})

Twig.extendFilter('anticache',
    (value) => {
        const stats = stat('build/' + value)
        return value + '?' + new Date(stats.mtime).getTime()
    }
)

Twig.extendFilter('anticache',
    (value) => {
        const stats = stat('build/' + value)
        return value + '?' + new Date(stats.mtime).getTime()
    }
)

const render = (file, parameters = {}) => {
    return new Promise((resolve, reject) => {
        Twig.renderFile(file, parameters, (err, html) => {
            if (err) {
                reject(err)
            }
            resolve(html)
        })
    })
}

const getMeta = (dir, file) => {
    return new Promise((resolve) => {
        render(dir + file)
            .then((html) => {
                const $ = cheerio.load(html)
                const node = $('.project')
                resolve({
                    project: node.attr('project'),
                    title: node.attr('product'),
                    link: file.split('.')[0],
                })
            })
    })
}

const findGroups = (dir) => {
    return new Promise((resolve) => {
        fs.readdir(dir)
            .then((files) => {
                const promises = []
                files.forEach((file) => {
                    promises.push(getMeta(dir, file))
                })
                return Promise.all(promises)
            })
            .then((metaData) => {
                const groups = []
                metaData.forEach((item) => {
                    const id = item.link.split('-')[0]
                    if (!groups[id]) {
                        groups[id] = {
                            children: [],
                            name: item.project,
                        }
                    }
                    groups[id].children.push({
                        title: item.title,
                        link: item.link,
                    })
                })
                resolve(groups)
            })
    })
}

const fixTypography = (html) => {
    const processedHtml = html
    const $ = cheerio.load(
        processedHtml,
        { decodeEntities: false }
    )
    $('.project-text p').each((i, item) => {
        let paragraphHtml = $(item).html()
        paragraphHtml = tp.execute(paragraphHtml)
        paragraphHtml = perenoska(paragraphHtml)
        $(item).html(paragraphHtml)
    })
    return $.html()
}

module.exports = {
    findGroups: findGroups,
    render: render,
    fixTypography,
}
