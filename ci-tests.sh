#!/bin/bash
red='\033[0;31m'
green='\033[0;32m'
greenU='\033[4;32m'
nc='\033[0m'   

stage1() {
    echo -e "${greenU}1. Codestyle${nc}"
    npm run check-codestyle
    return $?
}

stage2() {
    echo -e "${greenU}2. Build${nc}"
    npm run build
    return $?
}

for i in {1..2}
do
    rm -rf /node_modules
    rm -rf /build
    npm install
    stage${i}
    check=$?
    if [ "$check" -eq 0 ]; then
        echo -e "${green}Success${nc}"
    else
        echo -e "${red}Error${nc}"
        exit 1
    fi
done
